# Junior Javascript Developer

- [Junior Javascript Developer](#junior-javascript-developer)
- [Web Development RoadMap](#web-development-roadmap)
- [Education Plan](#education-plan)
  - [Lecture 01. Developer environment](#lecture-01-developer-environment)
  - [Lecture 02. Grow Hacking in Action. Grow Own LinkedIn Social Network](#lecture-02-grow-hacking-in-action-grow-own-linkedin-social-network)
  - [Lecture 03. Unicat.SE Project Planning Session](#lecture-03-unicatse-project-planning-session)
  - [Lecture 04. Command Line Interface](#lecture-04-command-line-interface)
  - [Lecture 05. Integrated Developer Environment (IDE)](#lecture-05-integrated-developer-environment-ide)
  - [Lecture 06. VsCode Debugger](#lecture-06-vscode-debugger)
  - [Lecture 07. Puppeteer Project Run/Debug Session](#lecture-07-puppeteer-project-rundebug-session)
  - [Lecture 08. GIT and GIT Flow](#lecture-08-git-and-git-flow)
  - [Lecture 09. TDD, BDD with CucumberJs](#lecture-09-tdd-bdd-with-cucumberjs)
  - [Lecture 10. Code review of Instagram posts scrapper project](#lecture-10-code-review-of-instagram-posts-scrapper-project)
  - [Lecture 11. HTML, CSS, JavaScript + React](#lecture-11-html-css-javascript--react)
- [JavaScript](#javascript)
- [Databases](#databases)
- [HTML5, CSS3](#html5-css3)
- [Gatsby](#gatsby)
- [Books](#books)
    - [GRASP, SOLID](#grasp-solid)

# Web Development RoadMap

- [![Web Developer Roadmap 2020](./images/MAP_web_development_in_2020.png)](https://coggle.it/diagram/XfeRbWj7xy3dsEX8/t/web-development-in-2020)
- [PDF. Web Developer Roadmap 2021](files/Web_Development_2021_Roadmap.pdf) - Open document to get access to all links, tools lists, etc.
- [![](https://img.youtube.com/vi/181VnUUJq-Y/0.jpg)](https://www.youtube.com/watch?v=181VnUUJq-Y "JavaScript development 2021")

# Education Plan

- Developer Environment
- GIT and Git Flow
- node.js, Comman Line Utilities, CLI
- Unit Testing: mocha, sinon, jest
- TDD, BDD with Cucumber.js
- Gatsby.js
- Firebase
- Firebase Functions


## Lecture 01. Developer environment

[![](https://img.youtube.com/vi/111BK5oODSU/0.jpg)](https://www.youtube.com/watch?v=111BK5oODSU "Lecture 01. Developer Environment")

Open [Presentation Materials](https://docs.google.com/presentation/d/1n12a7V5UFxghZ5538ALf5yyNx0z-lyXjwJbF-QixFTA/edit?usp=sharing) for getting access to links and comments of the slides.

## Lecture 02. Grow Hacking in Action. Grow Own LinkedIn Social Network

[![](https://img.youtube.com/vi/I_ZxQEcLJRo/0.jpg)](https://www.youtube.com/watch?v=I_ZxQEcLJRo "Lecture 02. Growth Hacking in Action")

Open [Presentation Materials](https://docs.google.com/presentation/d/1W7F5BJ5r7gqOHUDbXTKRmctI0sLCAd4RF7QfW8udykE/edit?usp=sharing) - Growth Hacking in Action.

TO-DO:
- Compose your Target Audience (who is the most beneficial to connect)
- Compose invite message, publish it here for review and suggestions, comments
- Compose follow-up message, publish for review

## Lecture 03. Unicat.SE Project Planning Session

[![](https://img.youtube.com/vi/jCjpt9Nu67k/0.jpg)](https://youtu.be/watch?v=jCjpt9Nu67k)

задачи после пленинга:
- выписать утилиты и попробовать каждую из них, прочитать об каждой утилите, чтобы было понимание что это такое
выписать утилиты и на каждую утилиту найти топ-3 альтернативы, выписать отдельной графой стоимость платного аккаунта утилиты (смотреть в смалл бизнесс, 1 сайт)
- просмотреть пару вступительных лекций о SEO (like: https://youtu.be/MYE6T_gd7H0)
- ознакомится со списком современных утилит в SEO для расширения кругозора выписать какие из утилит доступны для установки на свой компьютер, а какие только онлайн
  - [![Best SEO Tools 2021](https://img.youtube.com/vi/iT8kM9eErgE/0.jpg)](https://www.youtube.com/watch?v=iT8kM9eErgE "Best SEO Tools 2021 — 🥊 Tested and Compared")
  - [![Free SEO Tools 2021](https://img.youtube.com/vi/u3Q-_A5lT7c/0.jpg)](https://www.youtube.com/watch?v=u3Q-_A5lT7c "My 9 Favorite Free SEO Tools For 2021")
  - [![Free SEO Tools 2021](https://img.youtube.com/vi/p-k2RWRSN2k/0.jpg)](https://www.youtube.com/watch?v=p-k2RWRSN2k "7 Best Free SEO Tools for 2021 (+ How to Use Them)")
  - [![](https://img.youtube.com/vi/gNNlhkiU1SI/0.jpg)](https://www.youtube.com/watch?v=gNNlhkiU1SI "10 Best AI Tools for SEO Success in 2021")
  - [![](https://img.youtube.com/vi/5h4Wyi3jZzs/0.jpg)](https://www.youtube.com/watch?v=5h4Wyi3jZzs "SEO Tools - The Only 4 Tools You Need to Rank #1 in Google")
- Книга обязательная к прочтению: http://korden.ru/uploads/zmot_ru.pdf (75 страниц)
- и на последок тем кто хочет очень хорошо разбираться в SEO - вот вам план самообучения - https://docs.google.com/document/d/1oDyQtFbBPt--UYpCqzEnu9nJnjoqxZJpnk9ZlOZLuPA/edit?usp=sharing
следуя правилам плана за 45 дней можно стать экспертом в SEO.
- SEO - mind map's, for quick navigation:
  - [![SEO: Mind Map](./images/SEO_mind_map.png)](https://coggle.it/diagram/YDn0tuANbXtvP4JH/t/seo/777c19222bbdbf6284e773f2f8208245b7765701937f5e3d548a3aa12490ab30)
  - [![SEO: Practicies](./images/SEO_practices.png)](https://coggle.it/diagram/WxTm4Z3qXFmCcBX0/t/seo-practices)
  - [![SEO: Site Structure](./images/SEO_site_structure.png)](https://coggle.it/diagram/W3GOEwdQOWC6ivla/t/seo-site-structure)
  - [![SEO: On-site Content](./images/SEO_onsite_content.png)](https://coggle.it/diagram/WO8v5vzHbAABwtFl/t/seo)
- Free Domain Name: 
  - https://www.freenom.com/, 
  - http://www.dot.tk/, 
  - http://getfreedomain.name/, 
  - https://neilpatel.com/blog/how-to-get-a-free-domain/

## Lecture 04. Command Line Interface

[![](https://img.youtube.com/vi/av0bhgFKHQw/0.jpg)](https://www.youtube.com/watch?v=av0bhgFKHQw "Terminal, ZSH and CLI tools")

Open [Presentation Materials](https://docs.google.com/presentation/d/1EREvS-_Bi4xBuvPmVcm07LbEUQRXXWbW8KaKQcG0kyM/edit?usp=sharing) for links.

My mac configuration: https://gist.github.com/OleksandrKucherenko/e76220f22359e0e49c81c5474b1457a1

Extra materials:
- [![](https://img.youtube.com/vi/oxuRxtrO2Ag/0.jpg)](https://www.youtube.com/watch?v=oxuRxtrO2Ag "Beginners Guide to the Bash Terminal")
- [![](https://img.youtube.com/vi/v-F3YLd6oMw/0.jpg)](https://www.youtube.com/watch?v=v-F3YLd6oMw "Shell Scripting Crash Course")
- https://github.com/romkatv/powerlevel10k
- [![](https://img.youtube.com/vi/D7Em1wjMiak/0.jpg)](https://www.youtube.com/watch?v=D7Em1wjMiak "Install WSL 2.0")
- Linux terminals: https://opensource.com/article/21/2/linux-terminals
- [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/#use-different-keys-for-different-repositories)
- [How Passwordless SSH login works](https://betterprogramming.pub/how-passwordless-ssh-login-works-711cb9af235)
- just FYI: https://github.com/alebcay/awesome-shell (Optional thing)
- [Test Your BASH Skills By Playing Command Line Games](https://ostechnix.com/test-your-bash-skills-by-playing-command-line-games/)
- [Linux Comman Line Cheat Sheet](https://cheatography.com/davechild/cheat-sheets/linux-command-line/)

вопросы для самопроверки по command line utils:
- как создать директорию? как создать несколько директорий? - как создать вложенные директории?
- как узнать текущую директорию?
- как вывести содержимое текстового файла? как вывести содержимое файла постранично?
- как найти слово в файле? как вывести все строки из файла которые содержат определенное слово?
- как найти строки которые содержат несколько слов поиска?
- как посортировать строки в файле?
- как вывести только уникальные строки из файла?
- как найти все файлы с определенным именем? как найти директории с заданным именем?
- как узнать и вывести размер файла? как вывести размер директории?
- как создать пустой файл? как переписать содержимое файла? - как сбросить размер файла до нуля?
- как удалить файл? как удалить пустую директорию? как удалить директории с файлами и под-директориями?
- как переименовать файл? как переименовать директорию?
- как вывести только первых 10 строк файла? как вывести 10 последних строк файла? как вывести с 10 по 100 строчку файла?
- как посчитать количество строк в файле?
- как посчитать хеш файла (md5, sha1)?
- как удалить все файлы рекурсивно по всем директориям по имени?
- как создать переменную окружения? как записать путь к папке пользователя через переменные?
- как получить timestamp (время в формате yyyymmdd_hhmmss)?
- как склеить содержимое двух файлов? нескольких файлов?
- как посчитать количество повторений строк в файле?
- как заменить слово с ошибкой на правильное в файле? как заменить "якорь" (ключевое слово) в файле шаблоне на значение глобальное переменной?
- как вывести дерево директорий проекта?

практические задачи:
- как найти все файлы-картинки которые используются в проекте?
- как найти все вызовы метода t(...) в проекте? посортировать? найти только уникальные?
- как склеить содержимое нескольких JSON файлов? CSV файлов? XML файлов?
- как создать бекап (резервную копию) файла?
- как сделать архив проекта?
- как вывести содержимое ключа/проперти из JSON файла?

## Lecture 05. Integrated Developer Environment (IDE)

[![](https://img.youtube.com/vi/kLIn6-XfCvs/0.jpg)](https://www.youtube.com/watch?v=kLIn6-XfCvs "IDE, simple project")

Open [Presentation Materials](https://docs.google.com/presentation/d/1GJiZJv-OrIlim-lw7V9rzfBQveLMghHz9Xu5pUYpbdw/edit?usp=sharing) for links and comments.

based on project: https://gitlab.com/unicat.se/www/-/tree/master/automation

code of lecture: https://gitlab.com/unicat.se/www/-/tree/scrapper-subproject/subprojects/scrapper

done project: https://gitlab.com/olku/puppeeter-data-scraper

Контрольные вопросы для самопроверки:
- можно ли создавать проекты внутри других проектов? как это влияет на под-проекты?
- какая последовательность действия создания нового проекта?
- как находятся библиотеки и альтернативы?
- что такое TypeScript?
- как получить список всех библиотек проекта?
- куда записываются "зависимости" на глобально установленные библотеки? почему их не стоит использовать?
возможно ли добавить несколько версий одной и тойже библиотеки в проект?
- как создать кастомную команду коммандной строки для yarn?
- как включить автоформатирование кода в момент записи файла?
- как запустить форматирование кода через коммандную строку?
- как запустить тесты через коммандную строку?
- как установить зависимости проекта?
- как запустить только один конкретный тест из набора всех тестов?
- что такое Destructuring assignment ?
- что такое модуль в node.js ?
- как экспортировать массив констант из модуля?
- как записать функцию в форме лямда выражения? (arrow function expression)
- что такое async function?
- какими спосабами можно задекларировать функцию?
- что такое область видимости? global vs local
- что такое eslint? prittier?
- Arrow Functions vs Traditional Functions in JavaScript... в чем разница?
- что такое given/when/then? чем важна такая структура записи теста? Какие еще есть варианты? Что такое AAA-pattern?
- что такое MOCK? STUB? FAKE? SPY? DUMMY?
- как отключить определенный тест в коде не комментируя его?
- как проверить какие зависимости у модуля/библиотеки? 
- как обновить библиотеки в проекте? как обновить только одну единственную библиотеку? 
- как зафиксировать версию библиотеки? как понизить (downgrade) версию библиотеки?
- где искать CLI утилити от разных библиотек в проекте? где короткие имена их?

Extra materials:
- [Visualization of NPM dependencies](http://npm.anvaka.com/#/view/2d/yargs)
- Visual Studio Code plugins:
  - List all plugins: `code --list-extensions` 
  - Results:
    ```text
    alefragnani.Bookmarks
    bierner.markdown-mermaid
    codezombiech.gitignore
    dawhite.mustache
    dbaeumer.vscode-eslint
    EditorConfig.EditorConfig
    esbenp.prettier-vscode
    imgildev.vscode-mustache-snippets
    k--kato.intellij-idea-keybindings
    mathiasfrohlich.Kotlin
    mechatroner.rainbow-csv
    PKief.material-icon-theme
    vscode-icons-team.vscode-icons
    yzhang.markdown-all-in-one
    ```
  - To install plugin use command: `code --install {plug_in_name}`
- [![](https://img.youtube.com/vi/c5GAS_PMXDs/0.jpg)](https://www.youtube.com/watch?v=c5GAS_PMXDs "10 Best VsCode Plugins")

## Lecture 06. VsCode Debugger

[![](https://img.youtube.com/vi/3V2XFkAhekk/0.jpg)](https://www.youtube.com/watch?v=3V2XFkAhekk "VsCode Debugger")

Articles:
- https://code.visualstudio.com/docs/editor/debugging

Extra materials:
- https://www.youtube.com/watch?v=2oFKNL7vYV8
- https://www.youtube.com/watch?v=llPW0b1dQms
- https://www.youtube.com/watch?v=yFtU6_UaOtA
- https://www.youtube.com/watch?v=6cOsxaNC06c

Event Loop / Call stacks:
- https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#what-is-the-event-loop
- https://nodejs.dev/learn/the-nodejs-event-loop
- https://blog.risingstack.com/node-js-at-scale-understanding-node-js-event-loop/

Scope:
- https://www.w3schools.com/js/js_scope.asp
- https://dmitripavlutin.com/javascript-scope/

Async/Await Under the Hood:
- https://medium.com/siliconwat/how-javascript-async-await-works-3cab4b7d21da
- https://dev.to/khaosdoctor/node-js-under-the-hood-3-deep-dive-into-the-event-loop-135d
- https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c

Javascript Testing:
- https://medium.com/welldone-software/an-overview-of-javascript-testing-7ce7298b9870
- Test launchers are used to launch your tests in the browser or Node.js with user config. ([Karma](https://karma-runner.github.io/latest/index.html), [Jasmine](https://jasmine.github.io/), [Jest](https://jestjs.io/), [TestCafe](https://github.com/DevExpress/testcafe), [Cypress](https://www.cypress.io/), [webdriverio](https://webdriver.io/))

- Testing structure providers help you arrange your tests in a readable and scalable way. ([Mocha](https://mochajs.org/), [Jasmine](https://jasmine.github.io/), [Jest](https://jestjs.io/), [Cucumber](https://github.com/cucumber/cucumber-js), [TestCafe](https://github.com/DevExpress/testcafe), [Cypress](https://www.cypress.io/))
  
- Assertion functions are used to check if a test returns what you expect it to return and if its’t it throws a clear exception. ([Chai](https://www.chaijs.com/), [Jasmine](https://jasmine.github.io/), [Jest](https://jestjs.io/), [Unexpected](http://unexpected.js.org/), [TestCafe](https://github.com/DevExpress/testcafe), [Cypress](https://www.cypress.io/))
  
- Generate and display test progress and summary. ([Mocha](https://mochajs.org/), [Jasmine](https://jasmine.github.io/), [Jest](https://jestjs.io/), [Karma](https://karma-runner.github.io/latest/index.html), [TestCafe](https://github.com/DevExpress/testcafe), [Cypress](https://www.cypress.io/))

- Mocks, spies, and stubs to simulate tests scenarios, isolate the tested part of the software from other parts, and attach to processes to see they work as expected. ([Sinon](https://sinonjs.org/), [Jasmine](https://jasmine.github.io/), [enzyme](https://enzymejs.github.io/enzyme/docs/api/), [Jest](https://jestjs.io/), [testdouble](https://testdouble.com/))

- Generate and compare snapshots to make sure changes to data structures from previous test runs are intended by the user’s code changes. ([Jest](https://jestjs.io/), [Ava](https://github.com/avajs/ava))

- Generate code coverage reports of how much of your code is covered by tests. ([Istanbul](https://gotwarlost.github.io/istanbul/), [Jest](https://jestjs.io/), [Blanket](https://github.com/alex-seville/blanket))

- Browser Controllers simulate user actions for Functional Tests. ([Nightwatch](https://nightwatchjs.org/), [Nightmare](https://github.com/segmentio/nightmare), [Phantom](https://phantomjs.org/), [Puppeteer](https://github.com/puppeteer/puppeteer), [TestCafe](https://github.com/DevExpress/testcafe), [Cypress](https://www.cypress.io/))

- Visual Regression Tools are used to compare your site to its previous versions visually by using image comparison techniques. ([Applitools](https://applitools.com/), [Percy](https://percy.io/), [Wraith](https://github.com/bbc/wraith), [WebdriverCSS](https://github.com/webdriverio-boneyard/webdrivercss))


Most popular in life productions: jest, cypress (alternatives: mocha, puppeteer, [storybook](https://github.com/storybookjs/storybook))

Keywords: WSL2, x410, cypress:
- [![](https://img.youtube.com/vi/yQsrQKQVg-k/0.jpg)](https://www.youtube.com/watch?v=yQsrQKQVg-k "Setting up WSL2 for web development")
- WSL2 , Windows Configuration - https://github.com/Alex-D/dotfiles

## Lecture 07. Puppeteer Project Run/Debug Session

[![](https://img.youtube.com/vi/IgfIRwRVqOg/0.jpg)](https://www.youtube.com/watch?v=IgfIRwRVqOg "Puppeteer Project Run/Debug Session")

Tasks:
- take project: https://gitlab.com/olku/puppeeter-data-scraper
- start project under debugger and execute line-by-line, try to understand what each line is doing

Goals:
- learn how to create a simple web browser automation tool
- create tool that will create “PhantomBuster.com trial account” from CLI

Needed skills:
- regex in Javascript
- DOM/CSS selectors:
  - https://vegibit.com/css-selectors-tutorial/, 
  - https://www.w3.org/TR/selectors-api/, 
  - https://www.w3schools.com/cssref/css_selectors.asp

Extra materials:
- https://developers.google.com/web
- [![](https://img.youtube.com/vi/MbnATLCuKI4/0.jpg)](https://www.youtube.com/watch?v=MbnATLCuKI4 "Puppeteer Google IO")
- [![NodeJs and Express](https://img.youtube.com/vi/TNV0_7QRDwY/0.jpg)](https://www.youtube.com/watch?v=TNV0_7QRDwY "Node.JS & Express Tutorial")
- Javascript-джедай #1 - Введение. https://www.youtube.com/watch?v=H6G63NKRSi8&list=PL363QX7S8MfSxcHzvkNEqMYbOyhLeWwem&index=1
- Курс "React JS - путь самурая 1.0", уроки, практика. https://www.youtube.com/playlist?list=PLcvhF2Wqh7DNVy1OCUpG3i5lyxyBWhGZ8
- Ulbi TV - https://www.youtube.com/channel/UCDzGdB9TTgFm8jRXn1tBdoA

## Lecture 08. GIT and GIT Flow

[![](https://img.youtube.com/vi/k6mdLCDInxY/0.jpg)](https://www.youtube.com/watch?v=k6mdLCDInxY "GIT and GIT Flow")

Open [Presentation Materials](https://docs.google.com/presentation/d/1SneqlbEo3F5GorL6068ezLV4NvLMxm1EIJurAAG0ynk/edit?usp=sharing) for links and comments.

![Git Flow With Commands](images/GIT_flow_commands.png)

Extra materials:
- Different git usage strategies: https://nvie.com/posts/a-successful-git-branching-model/
- Example of the project initial documentation for developers about GIT setup: https://docs.google.com/document/d/1oKueL90nv3lKwXanNNcvXixjmfg_eH7zIIEWiJ3VfBI/edit?usp=sharing
- https://proglib.io/p/painful-git

EXTRAS:
> A “detached HEAD” message in git just means that HEAD (the part of git that tracks what your current working directory should match) is pointing directly to a commit rather than a branch. Any changes that are committed in this state are only remembered as long as you don’t switch to a different branch. As soon as you checkout a new branch or tag, the detached commits will be “lost” (because HEAD has moved). If you want to save commits done in a detached state, you need to create a branch to remember the commits.

src: https://stackoverflow.com/q/19033709/349681

Tools:
- Fork Tool - https://fork.dev/
- alternatives: https://git-scm.com/download/gui/windows
- another tool for mac: https://gitup.co/
- VsCode also has extensions for GIT: git history, gitlens, git graph
- `brew install --cask gitup`
- https://www.syntevo.com/smartgit/ - its free for non-commercial use
- очень похож на fork https://www.gitkraken.com/
- you can try https://www.cycligent.com/git-tool
- [GIT Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)
- Quokka, boost initial education in JavaScript - https://youtu.be/l-zB8k582k8

Контрольные вопросы по лекции:
- зачем используют стратегию форк? какие бонусы? какие минусы?
- каким образом можно решить мердж конфликт без специальной утилиты diff?
- как подключить несколько репозиториев к одному проекту? upstream, origin
- чем отличается контроль версий файла от операций Undo/Redo редактора текста?
- как удалить файл из под контроля джита? обратная операция от git add <file>
- какую информацию хранит джит в каждом коммите?
- что такое таг? для чего его часто используют?
- как джит хуки должны быть настроены чтобы не разрешать девелоперу коммиты без комментария длиннее 120 символов?
записываются(попадают) ли настройки гит хуков под контроль изменений?
- можно ли сделать мердж бранча мастер (master|main) в ветку фичи? можно ли сразу после этого сделать мердж фичи бранча в мастер? в чем проблема такой последовательности действий?
- как востановить начальное состояние проекта до состояния серверной копии?
- каким образом джит знает какие файлы исключать из коммита? - могут ли правила меняться внутри под-фолдера?
- что такое короткий хеш и длинный хеш коммита?
- как откатить последние изменения (последний коммит)?
- как добавить забытый файл в последний коммит?
- как исключить файл из коммита?
- как поменять комментарий коммита?
- как поменять имя ветки?
- как поменять имя тага?
- как создать ветку от ветки? как создать ветку от какого-то коммита по хешу?
- что такое HEAD ? FETCH_HEAD, ORIGIN_HEAD, REBASE_HEAD
- что такое dead branch?
- как удалить все файлы которые игнорируются джитом в проекте? как узнать что будет реально удаляться?

## Lecture 09. TDD, BDD with CucumberJs

[![](https://img.youtube.com/vi/BPSXryTeL9g/0.jpg)](https://www.youtube.com/watch?v=BPSXryTeL9g "BDD, TDD")

source code: https://gitlab.com/unicat.se/www/-/compare/master...cucumber-js-bdd

Extra materials:
- a little about sinon stub/mock - https://minaluke.medium.com/how-to-stub-spy-a-default-exported-function-a2dc1b580a6b
- Write your own CucumberJs Features and Steps: 
  - [![Write your own CucumberJs Features and Steps](https://img.youtube.com/vi/V-G8EnDlHxg/0.jpg)](https://www.youtube.com/watch?v=V-G8EnDlHxg "Write your own CucumberJs Features and Steps")
- https://medium.com/@altshort/unit-testing-node-cli-apps-with-jest-2cd4adc599fb
 
## Lecture 10. Code review of Instagram posts scrapper project

[![](https://img.youtube.com/vi/LALDqMnpa0M/0.jpg)](https://www.youtube.com/watch?v=LALDqMnpa0M "Code review")

Extra materials:
- [![](https://img.youtube.com/vi/-SY5PP50aw0/0.jpg)](https://www.youtube.com/watch?v=-SY5PP50aw0 "NodeJs Scrapper with Puppeteer")
- [![](https://img.youtube.com/vi/4UMOn9KchbM/0.jpg)](https://www.youtube.com/watch?v=4UMOn9KchbM "AutoTests. Modular testing.")
- [![](https://img.youtube.com/vi/Z3w6eMq7etw/0.jpg)](https://www.youtube.com/watch?v=Z3w6eMq7etw "What is Testing")
- [![](https://img.youtube.com/vi/IEDe8jl5efU/0.jpg)](https://www.youtube.com/watch?v=IEDe8jl5efU "JavaScript unit tests with Jest")
- [![BDD & TDD](images/BDD_tdd_overview.png)](https://medium.com/@andr.ivas12/%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B4%D0%BB%D1%8F-%D1%87%D0%B0%D0%B9%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2-c007d43da791)

## Lecture 11. HTML, CSS, JavaScript + React

[![](https://img.youtube.com/vi/U3GxlWOnidw/0.jpg)](https://www.youtube.com/watch?v=U3GxlWOnidw "HTML, CSS, JS and React")

[Presentation](https://docs.google.com/presentation/d/1QQlmbq0XFt35okFa_ev9d54hafENycfBxpza70icG7I/edit?usp=sharing)

Extra materials:
- [HTML Tags Chearsheet](https://digital.com/tools/html-cheatsheet/)
- [How many CSS properties are there?](https://css-tricks.com/how-many-css-properties-are-there)
- [CSS Seclectors Tutorial](https://vegibit.com/css-selectors-tutorial/)
- [CSS Almanac](https://css-tricks.com/almanac/)
- [DOM](https://medium.com/swlh/working-on-dom-nodes-and-its-properties-in-javascript-af3ac77f803)
- [DOM & BOM](https://medium.com/@fknussel/dom-bom-revisited-cf6124e2a816)
- [Html Element Events](https://www.tutorialrepublic.com/javascript-tutorial/javascript-event-propagation.php)
- https://stackoverflow.com/a/4616720
- [Events Order](https://www.quirksmode.org/js/events_order.html)
- [DOM events w3c standard](https://www.w3.org/TR/DOM-Level-2-Events/events.html)
- [Handling Events](https://reactjs.org/docs/handling-events.html)
- [Virtual DOM, VDOM](https://medium.com/@rajaraodv/the-inner-workings-of-virtual-dom-666ee7ad47cf)
- [React Lifecycle](https://vmarchesin.medium.com/the-react-lifecycle-step-by-step-47c0db0bfe73)
- [React Lifecycle in Details](https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459)
- [ReactJs Lifecycle](https://www.wikitechy.com/tutorials/react/reactjs-component-life-cycle)
- [Full React Lifecycle with error phase](https://www.quora.com/What-is-the-use-of-life-cycle-methods-in-ReactJS)
- [React Hooks Lifecycle](https://github.com/Wavez/react-hooks-lifecycle)
- [DOM & BOM Revisited](https://medium.com/@fknussel/dom-bom-revisited-cf6124e2a816)

Self Verify Questions:
- What objects become available for developer from BOM (Browser Object Model)?
- Class Definition vs Object vs Instance ? What is the difference?
- What is Object Model (OM)?
- What is API (Application Programming Interface)? How is that connected to Object Model?
- Why needed hooks in React? 
- On which stage of lifecycle hooks are applied?
- Which API provides `window` object of BOM?
- Which API provides `document` object of BOM?
- Which API provides `screen` object of BOM?
- Which API provides `navigator` object of BOM?
- Which API provides `location` object of BOM?
- Which API provides `history` object of BOM?
- Which API provides `frames` object of BOM?
- What is BOM Timing Events?
- JavaScript vs DOM vs BOM? What is the difference?
- Document Object Model (DOM) API ?
  - appendChild / removeChild / replaceChild / insertBefore
  - createElement / createAttribute / createTextNode / createComment
  - removeNode
  - parentElement / parentNode
  - lastChild / firstChild / childNodes / previousSibling / nextSibling
  - getAttribute / setAttribute / removeAttribute
  - classList.add / classList.remove / classList.toggle/ classList.contains
  - addEventListener / removeEventListener
- Accessing nodes via DOM:
  - querySelector / querySelectorAll
  - getElementById
  - getElementByClassName
  - getElementsByTagName
  - getElementsByName

# JavaScript

- [Timus Shemsedinov YouTube Channel](https://www.youtube.com/channel/UChSGI2R2kRMjzXJuYqHWQZg)
- [Learning JavaScript eBook in Russian](files/Learning%20JavaScript%20(Russian).PDF)
- [Kevin Powel YouTube Channel](https://www.youtube.com/kepowob/videos)

# Databases

just FYI, https://antonz.org/sqlite-is-not-a-toy-database/

# HTML5, CSS3

[![](https://img.youtube.com/vi/V5rIPKzcX7Q/0.jpg)](https://www.youtube.com/watch?v=V5rIPKzcX7Q "HTML5 and CSS3")

# Gatsby 

- https://www.udemy.com/course/gatsby-js-firebase-hybrid-realtime-static-sites/ 
- https://www.udemy.com/course/gatsby-tutorial-and-projects-course


# Books

- Code Complete: A Practical Handbook of Software Construction, Second Edition
  - https://www.amazon.com/gp/product/0735619670/
  - The book is full of great advice about how to write good code regardless of architecture or programming language. Code Complete goes into the details of the structure of writing good code.
- Clean Code: A Handbook of Agile Software Craftsmanship
  - https://www.amazon.com/gp/product/0132350882
  - This is another one of those books that completely changed the way I wrote code. I can neatly divide my programming career into pre-Code Complete, pre-Clean Code, and after. I recommend this book after reading Code Complete because while Code Complete deals more with the structure of individual lines of code and methods, Clean Code deals with some of the same concepts but at a slightly higher level.
- Design Patterns: Elements of Reusable Object-Oriented Software
  - https://www.amazon.com/gp/product/0201633612
  - This classical book is critical reading to really understand what design patterns are and become familiar with the most common design patterns you are likely to encounter in your career. It’s not a particularly easy read, and the descriptions and examples might be a bit difficult to follow—especially if you don’t have a solid grasp of UML
- Refactoring: Improving the Design of Existing Code
  - https://www.amazon.com/gp/product/0201485672
  - Although modern IDEs have automated many of the refactorings mentioned in this book, refactoring is still a very important concept to understand in order to write good, clean code—especially in today’s Agile environments. This book covers just about all of the major refactorings that all software developers should know how to execute in any code base.
- The Art of Computer Programming
  - https://www.amazon.com/gp/product/0321751043
  - Can I really recommend a book series I’ve never read? Sure can. This four-volume set was on my list of “someday to read” books, but I never got around to it.
Why? Because reading these books is a huge undertaking. These books go through, in depth and in detail, computer science algorithms—and not the easy stuff.
  - Not recommended for purchase. Better to use the next book
- Introduction to Algorithms, 3rd Edition (The MIT Press)
  - https://www.amazon.com/dp/0262033844/
  - This book is considered one of the best books on learning algorithms, and for good reason. It is a solid programming book for anyone interested in increasing their ability to write and understand algorithms, which is the core of writing code.
- Object Oriented Analysis And Design With Applications
  - https://www.amazon.com/dp/8131722872
  - It provides thorough and practical coverage of concepts, techniques, notations and examples for modern object-oriented analysis and design. The material cov-ered draws upon a solid foundation of theoretical work but is con-sistently pragmatic in approach. This book provides an essential body of knowledge for professionals responsible for the analysis and design of complex systems.

все книги есть в переводе и их можно заказать:
- https://www.ozon.ru/context/detail/id/3905587/
- https://www.yakaboo.ua/objectoriented-analysis-and-design-with-application.html
- https://booksonline.com.ua/view.php?book=140875

### GRASP, SOLID

[![](https://img.youtube.com/vi/pswA3Prf_YQ/0.jpg)](https://www.youtube.com/watch?v=pswA3Prf_YQ "GRASP")

[![](https://img.youtube.com/vi/ExauFjYV_lQ/0.jpg)](https://www.youtube.com/watch?v=ExauFjYV_lQ "GRASP")